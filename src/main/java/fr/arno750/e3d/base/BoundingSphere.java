package fr.arno750.e3d.base;

import lombok.Getter;

import java.util.List;

@Getter
public class BoundingSphere {

    public static double RADIUS_INCREASE = 1.0001;
    protected Point3D center;
    protected double radius;

    public BoundingSphere(Point3D center, double radius) {
        this.center = center;
        this.radius = radius;
    }

    /**
     * Returns a bounding sphere based on minimum and maximum points.
     * <p/>
     * Retrieves the minimum and maximum values on each coordinate.
     * Defines the center of the bounding sphere as the middle of minimum and maximum points and the radius as half the distance between minimum and maximum points (here calculated as the distance between the center and the minimum point)
     * <p/>
     * The sphere defined by the center and radius is in effect a bounding sphere for the volume. It is not the optimum bounding sphere. This algorithm runs in time O(n) on inputs consisting of n points.
     * <p/>
     * The radius is increased by 0.01% in order to guard against precision problems on floating point numbers, especially when using <code>containsPoint()</code> and <code>containsVertices()</code>.
     *
     * @see <a href="https://en.wikipedia.org/wiki/Smallest-circle_problem">Smallest-circle problem</a>
     * @see <a href="https://en.wikipedia.org/wiki/Bounding_sphere">Bounding sphere</a>
     */
    public static BoundingSphere getBoundingSphere(List<Vertex> vertices) {
        Point3D min = new Point3D(vertices.get(0).p);
        Point3D max = new Point3D(vertices.get(0).p);
        for (Vertex v : vertices) {
            min.x = Math.min(min.x, v.p.x);
            min.y = Math.min(min.y, v.p.y);
            min.z = Math.min(min.z, v.p.z);
            max.x = Math.max(max.x, v.p.x);
            max.y = Math.max(max.y, v.p.y);
            max.z = Math.max(max.z, v.p.z);
        }

        Point3D center = Point3D.getMiddle(min, max);
        double radius = center.getDistance(min);
        return new BoundingSphere(center, radius * RADIUS_INCREASE);
    }

    /**
     * Works out a bounding sphere using Ritter's algorithm.
     * <p/>
     * This sphere is not the optimum bounding sphere but is usually close. This algorithm runs in time O(3n) on inputs consisting of n points in 3-dimensional space.
     * <p/>
     * The radius is increased by 0.01% in order to guard against precision problems on floating point numbers, especially when using <code>containsPoint()</code> and <code>containsVertices()</code>.
     *
     * @see <a href="https://en.wikipedia.org/wiki/Bounding_sphere">Bounding sphere</a>
     */
    public static BoundingSphere getRitterBoundingSphere(List<Vertex> vertices) {
        Vertex x = vertices.get(0);
        Vertex y = BoundingSphere.getFurthestVertex(vertices, x);
        Vertex z = BoundingSphere.getFurthestVertex(vertices, y);
        Point3D center = Point3D.getMiddle(y.p, z.p);
        double radius = y.p.getDistance(z.p) / 2.0;
        double squaredRadius = radius * radius;

        for (Vertex v : vertices) {
            double squaredDistance = v.p.getSquaredDistance(center);
            if (squaredDistance <= squaredRadius)
                continue;

            double distance = Math.sqrt(squaredDistance);
            radius = (radius + distance) / 2.0;
            squaredRadius = radius * radius;
            double weight = distance - radius;
            center.x = (center.x * radius + v.p.x * weight) / distance;
            center.y = (center.y * radius + v.p.y * weight) / distance;
            center.z = (center.z * radius + v.p.z * weight) / distance;
        }
        return new BoundingSphere(center, radius * RADIUS_INCREASE);
    }

    private static Vertex getFurthestVertex(List<Vertex> vertices, Vertex v0) {
        double maximum = 0;
        Vertex furthest = vertices.get(0);
        for (Vertex v : vertices) {
            double squaredDistance = v.p.getSquaredDistance(v0.p);
            if (squaredDistance > maximum) {
                maximum = squaredDistance;
                furthest = v;
            }
        }
        return furthest;
    }

    /**
     * Indicates whether the specified point is contained in the sphere.
     *
     * @param p a point.
     * @return <code>true</code> if contained ; <code>false</code> otherwise.
     */
    public boolean containsPoint(Point3D p) {
        return this.center.getSquaredDistance(p) <= radius * radius;
    }

    /**
     * Indicates whether a list of vertices is contained in the sphere.
     *
     * @param vertices a list of vertices.
     * @return <code>true</code> if contained ; <code>false</code> otherwise.
     */
    public boolean containsVertices(List<Vertex> vertices) {
        double squaredRadius = radius * radius;
        for (Vertex v : vertices) {
            if (this.center.getSquaredDistance(v.p) > squaredRadius)
                return false;
        }
        return true;
    }
}
