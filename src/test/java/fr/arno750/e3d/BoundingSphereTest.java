package fr.arno750.e3d;

import fr.arno750.e3d.base.BoundingSphere;
import fr.arno750.e3d.base.VertexFactory;
import fr.arno750.e3d.base.config.VolumeDefinition;
import fr.arno750.e3d.base.volume.Box;
import fr.arno750.e3d.base.volume.Sphere;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class BoundingSphereTest {

    @Test
    void givenBox_whenWorkOutBoundingSphere() {
        Box box = new Box(VolumeDefinition.getDefault(), new VertexFactory());
        BoundingSphere boundingSphere = BoundingSphere.getBoundingSphere(box.getVertices());
        assertEquals(Math.sqrt(0.75) * BoundingSphere.RADIUS_INCREASE, boundingSphere.getRadius());
        assertEquals(0.5, boundingSphere.getCenter().x);
        assertEquals(0.5, boundingSphere.getCenter().y);
        assertEquals(0.5, boundingSphere.getCenter().z);
        assertTrue(boundingSphere.containsVertices(box.getVertices()));
    }

    @Test
    void givenSphere_whenWorkOutBoundingSphere() {
        var vd = VolumeDefinition.getDefault();
        vd.getParameters().setLatitudes(32);
        vd.getParameters().setLongitudes(64);
        Sphere sphere = new Sphere(vd, new VertexFactory());
        BoundingSphere boundingSphere = BoundingSphere.getBoundingSphere(sphere.getVertices());
        assertEquals(Math.sqrt(3) * BoundingSphere.RADIUS_INCREASE, boundingSphere.getRadius());
        assertTrue(boundingSphere.containsVertices(sphere.getVertices()));
    }

    @Test
    void givenBox_whenWorkOutRitterBoundingSphere() {
        Box box = new Box(VolumeDefinition.getDefault(), new VertexFactory());
        BoundingSphere boundingSphere = BoundingSphere.getRitterBoundingSphere(box.getVertices());
        assertEquals(Math.sqrt(0.75) * BoundingSphere.RADIUS_INCREASE, boundingSphere.getRadius());
        assertEquals(0.5, boundingSphere.getCenter().x);
        assertEquals(0.5, boundingSphere.getCenter().y);
        assertEquals(0.5, boundingSphere.getCenter().z);
        assertTrue(boundingSphere.containsVertices(box.getVertices()));
    }

    @Test
    void givenSphere_whenWorkOutRitterBoundingSphere() {
        var vd = VolumeDefinition.getDefault();
        vd.getParameters().setLatitudes(32);
        vd.getParameters().setLongitudes(64);
        Sphere sphere = new Sphere(vd, new VertexFactory());
        BoundingSphere boundingSphere = BoundingSphere.getRitterBoundingSphere(sphere.getVertices());
        assertEquals(1.0 * BoundingSphere.RADIUS_INCREASE, boundingSphere.getRadius());
        assertTrue(boundingSphere.containsVertices(sphere.getVertices()));
    }
}
